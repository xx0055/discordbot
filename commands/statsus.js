import { SlashCommandBuilder, PermissionFlagsBits, ActivityType } from 'discord.js';
import { presenceOptions, changePresence } from "../app.js";

export const data = new SlashCommandBuilder()
    .setName('statsus')
    .setDescription('zmněn stav')
    .setDMPermission(false)
    .addBooleanOption(option => option.setName("reset")
        .setDescription("Chceš zresetovat na původní statsus?")
        .setRequired(true))
    .addStringOption(option => option.setName('name')
        .setDescription('Text na statsus')
        .setRequired(false))
    .addNumberOption(option => option.setName('type')
        .setDescription('Text na statsus')
        .setRequired(false)
        .addChoices(
            { name: 'Competing', value: 5 },
            { name: 'Listening', value: 2 },
            { name: 'Playing', value: 0 },
            { name: 'Streaming', value: 1 },
            { name: 'Watching', value: 3 }))
    /*.addStringOption(option =>
        option.setName('url')
            .setDescription('Text na status')
            .setRequired(false))*/
    .addStringOption(option => option.setName('status')
        .setDescription('jsi tu?')
        .setRequired(false)
        .addChoices(
            { name: 'online', value: "online" },
            { name: 'idle', value: "idle" },
            { name: 'invisible', value: "invisible" },
            { name: 'do not disturb', value: "dnd" }))
    .setDefaultMemberPermissions(PermissionFlagsBits.Administrator);
export async function execute(interaction) {
    await interaction.reply({ content: "Měním status bota...", ephemeral: false });
    if (await interaction.options.getBoolean("reset")) {
        await interaction.client.user.setPresence(presenceOptions);
        await interaction.editReply({ content: "Status bota byl resetován", ephemeral: true }).catch(err => console.log(err));
    } else {
        var name;
        var type;
        var status;
        var url;
        if (await interaction.options.getString('name') != undefined) {
            name = await interaction.options.getString('name');
            presenceOptions["activities"][0]["name"] = name;
        }
        if (await interaction.options.getNumber('type') != undefined) {
            type = await interaction.options.getNumber('type');
            if (type != 1)
                presenceOptions["activities"][0]["url"] = null;
            switch (type) {
                case 5: type = ActivityType.Competing; break;
                case 2: type = ActivityType.Listening; break;
                case 0: type = ActivityType.Playing; break;
                case 1: type = ActivityType.Streaming; break;
                case 3: type = ActivityType.Watching; break;
                default: console.log("error ig"); break;
            }
            presenceOptions["activities"][0]["type"] = type;
        }
        if (await interaction.options.getString('status') != undefined) {
            status = await interaction.options.getString('status');
            presenceOptions["status"] = status;
        }
        /*if(interaction.options.getString('url') != undefined){
            url = await interaction.options.getString('url');
            presenceOptions["activities"][0]["url"] = url;
        }*/
        //await interaction.client.user.setPresence(presenceOptions);
        changePresence(presenceOptions);
        await interaction.editReply({ content: "Status bota byl aktualizován", ephemeral: true }).catch(err => console.log(err));
    }
}