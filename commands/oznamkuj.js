import { SlashCommandBuilder, PermissionFlagsBits, EmbedBuilder } from 'discord.js';
import studenti from '../jsons/studenti.json' assert { type: "json" };
import vyucujici from '../jsons/vyucujici.json' assert { type: "json" };
import { writeFileSync } from 'node:fs';

export const data = new SlashCommandBuilder()
    .setName('oznamkuj')
    .setDescription('Oznámkuj jakéhokoliv učitele')
    .setDefaultMemberPermissions(PermissionFlagsBits.CreatePrivateThreads)
    .addStringOption(option => option.setName('kantor')
        .setDescription('Zadej novellovský jméno nebo celé jméno kantora (pomocí **/ucitel_find** si ověříš že to je fakt on)')
        .setRequired(true))
    .addNumberOption(option => option.setName('znamka')
        .setDescription('Zadej známku kterou by jste celkově kantora ohodnotili')
        .setChoices({ name: '1', value: 1 },
            { name: '2', value: 2 },
            { name: '3', value: 3 },
            { name: '4', value: 4 },
            { name: '5', value: 5 },
            { name: "Nevím/nemohu posoudit", value: 0 })
        .setRequired(true))
    .addNumberOption(option => option.setName('spravedl')
        .setDescription('Spravedlnost - dává známky podle toho jak jsi se oblékl, náhodně, nebo normálně')
        .setChoices({ name: '1', value: 1 },
            { name: '2', value: 2 },
            { name: '3', value: 3 },
            { name: '4', value: 4 },
            { name: '5', value: 5 },
            { name: "Nevím/nemohu posoudit", value: 0 })
        .setRequired(false))
    .addNumberOption(option => option.setName('kvalita')
        .setDescription('Kvalita - Naučí vás to co má?')
        .setChoices({ name: '1', value: 1 },
            { name: '2', value: 2 },
            { name: '3', value: 3 },
            { name: '4', value: 4 },
            { name: '5', value: 5 },
            { name: "Nevím/nemohu posoudit", value: 0 })
        .setRequired(false))
    .addNumberOption(option => option.setName('chovani')
        .setDescription('Chování - být zlý či hodný kantor náš? (není to to samé jako přísný)')
        .setChoices({ name: '1', value: 1 },
            { name: '2', value: 2 },
            { name: '3', value: 3 },
            { name: '4', value: 4 },
            { name: '5', value: 5 },
            { name: "Nevím/nemohu posoudit", value: 0 })
        .setRequired(false))
    .addNumberOption(option => option.setName('ochota')
        .setDescription('Ochota - když něco potřebuješ, pomůže?')
        .setChoices({ name: '1', value: 1 },
            { name: '2', value: 2 },
            { name: '3', value: 3 },
            { name: '4', value: 4 },
            { name: '5', value: 5 },
            { name: "Nevím/nemohu posoudit", value: 0 })
        .setRequired(false))
    .addNumberOption(option => option.setName('obtiznost')
        .setDescription('Obtížnost/Přísnost - Jak vysoké požadavky má na výuku?')
        .setChoices({ name: '1', value: 1 },
            { name: '2', value: 2 },
            { name: '3', value: 3 },
            { name: '4', value: 4 },
            { name: '5', value: 5 },
            { name: "Nevím/nemohu posoudit", value: 0 })
        .setRequired(false))
    .addStringOption(option => option.setName('poznamka')
        .setDescription('Zdali máš nějakou poznámku, tak povidaj')
        .setRequired(false))
    .setDMPermission(false);
export async function execute(interaction) {
    await interaction.reply({ content: "Zapisuju tvou odpověď...", ephemeral: true });
    var kantor = await interaction.options.getString('kantor');
    const znamka = await interaction.options.getNumber('znamka');
    const ochota = await interaction.options.getNumber('ochota');
    const chovani = await interaction.options.getNumber('chovani');
    const kvalita = await interaction.options.getNumber('kvalita');
    const spravedlnost = await interaction.options.getNumber('spravedl');
    const obtiznost = await interaction.options.getNumber('obtiznost');
    var poznamka = await interaction.options.getString("poznamka"); //.replace(/['"\\]+/g, ""));
    var student;
    
    const keyss = Object.keys(studenti);
    for (var i = 0; i < keyss.length; i++) {
        if (studenti[keyss[i]].fulljmeno === interaction.member.nickname || studenti[keyss[i]].fulljmeno === interaction.member.user.username) {
            student = keyss[i];
        }
    }
    if (!student) {
        await interaction.editReply({ content: "Hele hele... toto by se nemělo dít, napiš adminovi! ", ephemeral: true }).catch(err => console.log(err));
        return;
    }

    const keysv = Object.keys(vyucujici);
    for (var i = 0; i < keysv.length; i++) {
        if (vyucujici[keysv[i]].fulljmeno === kantor) {
            kantor = keysv[i];
            break;
        }
    }
    /*if (!vyucujici[kantor] || !vyucujici[kantor].checked) {
        await interaction.editReply({ content: "Tento učitel na škole již není, nemůžeš ho známkovat. ", ephemeral: true }).catch(err => console.log(err));
        return;
    }*/
    if (!vyucujici[kantor]) {
        await interaction.editReply("Kantora jsme nenašli...").catch(err => console.log(err));
    } else if (student) {
        await interaction.editReply("Našli jsme **" + vyucujici[kantor].fulljmeno + "**, teď zapisuju...").catch(err => console.log(err));
        if (!vyucujici[kantor].hodnoceni)
            vyucujici[kantor].hodnoceni = {};
        if (!vyucujici[kantor].hodnoceni.znamka)
            vyucujici[kantor].hodnoceni.znamka = {};
        if (!vyucujici[kantor].hodnoceni.ochota)
            vyucujici[kantor].hodnoceni.ochota = {};
        if (!vyucujici[kantor].hodnoceni.chovani)
            vyucujici[kantor].hodnoceni.chovani = {};
        if (!vyucujici[kantor].hodnoceni.kvalita)
            vyucujici[kantor].hodnoceni.kvalita = {};
        if (!vyucujici[kantor].hodnoceni.spravedlnost)
            vyucujici[kantor].hodnoceni.spravedlnost = {};
        if (!vyucujici[kantor].hodnoceni.obtiznost)
            vyucujici[kantor].hodnoceni.obtiznost = {};
        if (!vyucujici[kantor].hodnoceni.poznamka)
            vyucujici[kantor].hodnoceni.poznamka = {};
        const hodnoceni = vyucujici[kantor].hodnoceni;
        var znamkaCelkPocet = 0;
        var znamkaOchotaPocet = 0;
        var znamkaSpravedlnostPocet = 0;
        var znamkaChovaniPocet = 0;
        var znamkaObtiznostPocet = 0;
        var znamkaKvalitaPocet = 0;
        var znamkaCelkSoucet = 0;
        var znamkaOchotaSoucet = 0;
        var znamkaSpravedlnostSoucet = 0;
        var znamkaChovaniSoucet = 0;
        var znamkaObtiznostSoucet = 0;
        var znamkaKvalitaSoucet = 0;
        if (znamka == 0)
            hodnoceni.znamka[student] = undefined; else if (znamka)
            hodnoceni.znamka[student] = znamka; //[student] = {znamka,ochota,chovani,kvalita,spravedlnost,obtiznost,poznamka};
        if (ochota == 0)
            hodnoceni.ochota[student] = undefined; else if (ochota)
            hodnoceni.ochota[student] = ochota;
        if (chovani == 0)
            hodnoceni.chovani[student] = undefined; else if (chovani)
            hodnoceni.chovani[student] = chovani;
        if (kvalita == 0)
            hodnoceni.kvalita[student] = undefined; else if (kvalita)
            hodnoceni.kvalita[student] = kvalita;
        if (spravedlnost == 0)
            hodnoceni.spravedlnost[student] = undefined; else if (spravedlnost)
            hodnoceni.spravedlnost[student] = spravedlnost;
        if (obtiznost == 0)
            hodnoceni.obtiznost[student] = undefined; else if (obtiznost)
            hodnoceni.obtiznost[student] = obtiznost;
        if (poznamka === "0")
            hodnoceni.poznamka[student] = undefined; else if (poznamka)
            hodnoceni.poznamka[student] = poznamka;
        await interaction.editReply("Známky pro **" + vyucujici[kantor].fulljmeno + "** zapsané! Přepočítávám...").catch(err => console.log(err));
        setTimeout(async () => {
            for (var i = 0; i < keyss.length; i++) {
                if (hodnoceni.znamka[keyss[i]]) {
                    znamkaCelkSoucet += hodnoceni.znamka[keyss[i]];
                    znamkaCelkPocet++;
                }
                if (hodnoceni.spravedlnost[keyss[i]]) {
                    znamkaSpravedlnostSoucet += hodnoceni.spravedlnost[keyss[i]];
                    znamkaSpravedlnostPocet++;
                }
                if (hodnoceni.ochota[keyss[i]]) {
                    znamkaOchotaSoucet += hodnoceni.ochota[keyss[i]];
                    znamkaOchotaPocet++;
                }
                if (hodnoceni.chovani[keyss[i]]) {
                    znamkaChovaniSoucet += hodnoceni.chovani[keyss[i]];
                    znamkaChovaniPocet++;
                }
                if (hodnoceni.kvalita[keyss[i]]) {
                    znamkaKvalitaSoucet += hodnoceni.kvalita[keyss[i]];
                    znamkaKvalitaPocet++;
                }
                if (hodnoceni.obtiznost[keyss[i]]) {
                    znamkaObtiznostSoucet += hodnoceni.obtiznost[keyss[i]];
                    znamkaObtiznostPocet++;
                }
            }

            if (znamkaCelkPocet > 0)
                hodnoceni.znamkacelk = Math.round(znamkaCelkSoucet / znamkaCelkPocet * 100) / 100; else
                hodnoceni.znamkacelk = "Nehodnoceno";
            if (znamkaSpravedlnostPocet > 0)
                hodnoceni.spravedlnostcelk = Math.round(znamkaSpravedlnostSoucet / znamkaSpravedlnostPocet * 10) / 10; else
                hodnoceni.spravedlnostcelk = "Nehodnoceno";
            if (znamkaOchotaPocet > 0)
                hodnoceni.ochotacelk = Math.round(znamkaOchotaSoucet / znamkaOchotaPocet * 100) / 100; else
                hodnoceni.ochotacelk = "Nehodnoceno";
            if (znamkaChovaniPocet > 0)
                hodnoceni.chovanicelk = Math.round(znamkaChovaniSoucet / znamkaChovaniPocet * 100) / 100; else
                hodnoceni.chovanicelk = "Nehodnoceno";
            if (znamkaKvalitaPocet > 0)
                hodnoceni.kvalitacelk = Math.round(znamkaKvalitaSoucet / znamkaKvalitaPocet * 100) / 100; else
                hodnoceni.kvalitacelk = "Nehodnoceno";
            if (znamkaObtiznostPocet > 0)
                hodnoceni.obtiznostcelk = Math.round(znamkaObtiznostSoucet / znamkaObtiznostPocet * 100) / 100; else
                hodnoceni.obtiznostcelk = "Nehodnoceno";
            var send = "Takto jsi ohodnotil **" + vyucujici[kantor].fulljmeno + "**:\n" +
                "Celkové hodnocení: ---- **" + hodnoceni.znamka[student] + "**\n" +
                "Spravedlnost: ----------- **" + hodnoceni.spravedlnost[student] + "**\n" +
                "Kvalita výuky: ----------- **" + hodnoceni.kvalita[student] + "**\n" +
                "Chování vůči žákům: --- **" + hodnoceni.chovani[student] + "**\n" +
                "Obtížnost výuky: ------- **" + hodnoceni.obtiznost[student] + "**\n" +
                "Ochota: ----------------- **" + hodnoceni.ochota[student] + "**"
                + "\n\n**__Tvá poznámka__**:";
            if (hodnoceni.poznamka[student]) {
                if (hodnoceni.poznamka[student] != null && hodnoceni.poznamka[student] != "")
                    send += "\n__" + studenti[student].fulljmeno + "__: \"*" + hodnoceni.poznamka[student] + "*\"";
            } else
                send += "\n*---*";
            await interaction.editReply(send).catch(err => console.log(err));


            //zapsani do permanentniho kanalu
            var uciteleChannel = await interaction.guild.channels.fetch("1048348552469958775");
            await uciteleChannel.messages.fetch();
            var ucitelMessage;
            await uciteleChannel.messages.cache.forEach(async (message) => {
                if (message.embeds[0] && message.embeds[0].title === vyucujici[kantor].fulljmeno) {
                    ucitelMessage = message;
                    return;
                }
            });
            var cislo;
            if (vyucujici[kantor].cislo)
                cislo = vyucujici[kantor].cislo; else
                cislo = "null";
            const pocet = Object.keys(hodnoceni.znamka);
            var description = "Email: ------------------- **" + kantor + "@spseplzen.cz**\n" +
                "Pracovní číslo: ---------- **" + cislo + "**\n" +
                "\n" +
                "**Hodnocení učitelů žáky:**\n" +
                "Celkové hodnocení: ---- **" + hodnoceni.znamkacelk + "**\n" +
                "Spravedlnost: ----------- **" + hodnoceni.spravedlnostcelk + "**\n" +
                "Kvalita výuky: ----------- **" + hodnoceni.kvalitacelk + "**\n" +
                "Chování vůči žákům: --- **" + hodnoceni.chovanicelk + "**\n" +
                "Obtížnost výuky: ------- **" + hodnoceni.obtiznostcelk + "**\n" +
                "Ochota: ----------------- **" + hodnoceni.ochotacelk + "**" +
                "\n\n**__Poznámky našich hlasujících__**:";
            if (hodnoceni.poznamka) {
                const poznamkujici = Object.keys(hodnoceni.poznamka);
                poznamkujici.forEach(async (poznamkovac) => {
                    if (hodnoceni.poznamka[poznamkovac])
                        description += "\n__" + studenti[poznamkovac].fulljmeno + "__: \"*" + hodnoceni.poznamka[poznamkovac] + "*\"";
                });
            } else
                description += "\n*Zatím tu nic není*";
            const post = new EmbedBuilder()
                .setColor(39423)
                .setTitle(vyucujici[kantor].fulljmeno)
                .setDescription(description)
                .setFooter({ text: "Hodnotilo " + pocet.length + " lidí" });
            if (ucitelMessage) {
                ucitelMessage.edit({ embeds: [post] });
            } else {

                await uciteleChannel.send({ embeds: [post] });
            }
            writeFileSync("./jsons/vyucujici.json", JSON.stringify(vyucujici));
        }, 100);
    } else
        await interaction.editReply("Hele nenašli jsme tě, obrať se s tím ke správce :/").catch(err => console.log(err));
}