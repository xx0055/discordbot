import { SlashCommandBuilder, PermissionFlagsBits } from 'discord.js';
import { writeFileSync } from 'node:fs';
import skupiny from "../jsons/skupiny.json" assert { type: "json" };

export const data = new SlashCommandBuilder()
    .setName('trida_manage')
    .setDescription('Managment tříd/skupin')
    .addStringOption(option => option.setName('trida')
        .setDescription('Zadej třídu (Jen písmena, čísla a tečka (npř. 69.XXX))')
        .setRequired(true))
    .addBooleanOption(option => option.setName("smazat")
        .setDescription("True - vymaže třídu / False - vytvoří pokud není, pokud je tak přepíše")
        .setRequired(false))
    .addBooleanOption(option => option.setName("overwrite")
        .setDescription("přepne overwrite")
        .setRequired(false))
    .addStringOption(option => option.setName('tridni_ucitel')
        .setDescription('Zadej třídního učitele (celé jméno podle **/ucitel_find**)')
        .setRequired(false))
    .addStringOption(option => option.setName('zamereni')
        .setDescription('Odborné zaměření')
        .setRequired(false)
        .addChoices(
            { name: 'Elektro', value: "26-41-M/01 Elektrotechnika" },
            { name: 'Elektro - TMD', value: "26-41-M/01 Elektrotechnika/TMD" },
            { name: 'Elektro - IOT', value: "26-41-M/01 Elektrotechnika IOT" },
            { name: 'IT - VASS', value: "18-20-M/01 Informační technologie - VASS" },
            { name: 'IT - ITFP', value: "18-20-M/01 Informační technologie - ITFP" },
            { name: 'Liceum - LOL - Imagine', value: "78-42-M/01 Technické lyceum" }))
    .addNumberOption(option => option.setName('ucebna')
        .setDescription('Zadej číslo učebny (npř. 1205)')
        .setRequired(false))
    .setDMPermission(false)
    .setDefaultMemberPermissions(PermissionFlagsBits.Administrator);
export async function execute(interaction) {
    await interaction.reply("Pracuju...");
    const trida = await interaction.options.getString('trida').replace(/([^a-zA-Z.0-9]+)/gi, '-');
    const del = await interaction.options.getBoolean('smazat');
    const ow = await interaction.options.getBoolean('overwrite');
    const ucitel = await interaction.options.getString('tridni_ucitel');
    const obor = await interaction.options.getString('zamereni');
    const ucebna = await interaction.options.getNumber('ucebna');
    if (trida.length > 6) {
        await interaction.editReply("Maximální délka třídy je 6,9!");
        return;
    }
    const tridaArr = trida.split(".");
    if (tridaArr.length != 2 || !/[0-9]/.test(tridaArr[0]) || !/[a-zA-Z]/.test(tridaArr[1])) {
        await interaction.editReply("Špatně zadaná třída **" + trida + "** - třida musí být ve formátu ***číslo*.*písmeno***!");
        return;
    }
    const tridaString = trida.toLowerCase().replace(".", "");
    if (del)
    if (skupiny[tridaString]) {
        skupiny[tridaString] = undefined;
        writeFileSync("./jsons/skupiny.json", JSON.stringify(skupiny));
        await interaction.editReply("Třída " + trida + " odstraněna!");
        return;
    } else {
        await interaction.editReply("Třída " + trida + " Nebyla nalezena!");
        return;
    }
    var replyText = "";
    var newTrida = false;
    if (!skupiny[tridaString]) { skupiny[tridaString] = {}; newTrida = true; }
    if(ow != undefined){
        skupiny[tridaString].ow = ow;
        replyText += ("*Třída " + trida + " přepnuta na OW: "+ow+"!*\n\n");
    }
    if (ucitel){
        skupiny[tridaString].ucitel = ucitel;
        replyText += ("*Třída " + trida + " učitel změněn na: "+ucitel+"!*\n\n");}
    if (obor){
        skupiny[tridaString].obor = obor;
        replyText += ("*Třída " + trida + " obor změněn na: "+obor+"!*\n\n");}
    if (ucebna){
        skupiny[tridaString].ucebna = ucebna;
        replyText += ("*Třída " + trida + " učebna změněna na: "+ucebna+"!*\n\n");}
    replyText += "Hotovo! - Třída **" + skupiny[tridaString].fulljmeno +
        "**:\nOverwrite: **" + skupiny[tridaString].ow +
        "**:\nTřídní učitel: **" + skupiny[tridaString].ucitel +
        ".**\nObor: **" + skupiny[tridaString].obor +
        ".**\nUčebna: **" + skupiny[tridaString].ucebna + ".**";
    if (newTrida)
        replyText += "\n***Tato třída byla právě vytvořena!***";
    await interaction.editReply(replyText).catch(err => console.log(err));
    writeFileSync("./jsons/skupiny.json", JSON.stringify(skupiny));
}