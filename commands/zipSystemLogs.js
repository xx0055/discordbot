import { SlashCommandBuilder, PermissionFlagsBits } from 'discord.js';
import { existsSync, mkdirSync, createWriteStream, readdir, unlink } from 'node:fs';
import archiver from "archiver";

export const data = new SlashCommandBuilder()
  .setName('zipsystemlogs')
  .setDescription('zip systemlogu')
  .setDMPermission(false)
  .setDefaultMemberPermissions(PermissionFlagsBits.Administrator);
export async function execute(interaction) {
  await interaction.reply({ content: "Vytvářím .zip soubor systémových logů...", ephemeral: true }).catch(err => console.log(err));
  const date = new Date(Date.now());
  var fileName = await "system.log-" + date.getFullYear() + "-" + date.getMonth() + "-" + date.getDay() + "_" + Date.now();
  const dir = "./backups_zips/system/";
  const backupDir = "./backups/system/";
  if (!existsSync("./backups_zips/")) {
    mkdirSync("./backups_zips/");
  }
  if (!existsSync(dir)) {
    mkdirSync(dir);
  }
  var output = createWriteStream(dir + fileName + ".zip");
  output.on('close', function () {
    console.log(archive.pointer() + ' total bytes');
    console.log('Backup kategorie ' + fileName + " byl vytvořen.");
  });
  var archive = archiver('zip');
  archive.pipe(output);
  archive.directory(backupDir, false);
  await archive.finalize();
  await interaction.editReply({ content: "Zip soubor vytvořen", ephemeral: true }).catch(err => console.log(err));
  var souborCount = 0;
  readdir(backupDir, async (err, files) => {
    if (err)
      throw err;
    await interaction.editReply({ content: "Mažu původní soubory...", ephemeral: true }).catch(err => console.log(err));
    files.forEach(async (file) => {
      unlink(backupDir + file, (err) => {
        if (err)
          throw err;
      });
      souborCount++;
    });
    await interaction.editReply({ content: "Zip soubor vytvořen a " + souborCount + " souborů vymazáno!", ephemeral: true }).catch(err => console.log(err));
  });
}