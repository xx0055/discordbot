import { SlashCommandBuilder, PermissionFlagsBits, ChannelType } from 'discord.js';
import studenti from '../jsons/studenti.json' assert { type: "json" };
import skupiny from '../jsons/skupiny.json' assert { type: "json" };
import roles from "../jsons/roles.json" assert { type: "json" };
import { writeFileSync } from 'node:fs';

export const data = new SlashCommandBuilder()
    .setName('student_set')
    .setDescription('Upraví hodnoty dané osoby')
    .setDefaultMemberPermissions(PermissionFlagsBits.Administrator)
    .addStringOption(option => option.setName('jmeno')
        .setDescription('To koho chceš zmněnit')
        .setRequired(true))
    .addStringOption(option => option.setName('parm')
        .setDescription('To co chceš zmněnit')
        .setRequired(true)
        .addChoices(
            { name: 'discord', value: "discord" },
            { name: 'trida', value: "trida" }))
    .addStringOption(option => option.setName('value')
        .setDescription('To na co to chceš zmněnit')
        .setRequired(true))
    .setDMPermission(false);
export async function execute(interaction) {
    await interaction.reply({ content: "Měním data...", ephemeral: true }).catch(err => console.error(err));
    const jmeno = await interaction.options.getString('jmeno');
    const parm = await interaction.options.getString('parm');
    const value = await interaction.options.getString('value');
    var student;
    var studentNovell;
    const keys = Object.keys(studenti);
    for (var i = 0; i < keys.length; i++) {
        if (studenti[jmeno] != undefined) {
            student = studenti[jmeno].discord;
            studentNovell = jmeno;
            break;
        }
        if (studenti[keys[i]].discord === jmeno) {
            student = studenti[keys[i]].discord;
            studentNovell = keys[i];
            break;
        }
        if (studenti[keys[i]].fulljmeno === jmeno) {
            student = studenti[keys[i]].discord;
            studentNovell = keys[i];
            break;
        }
    }

    if (!student && !studentNovell) { await interaction.editReply({ content: "Uživatel neexistuje", ephemeral: true }).catch(err => console.log(err)); return; }
    if (student)
        student = await interaction.guild.members.fetch(student.replace(/[@<>]/g, ""));
    if (studentNovell) { }
    //dicord handeling-----------------------
    try {
        if (parm === "discord")
            if (await interaction.guild.members.cache.get(value.replace(/[<>@]/g, "")) || value === "0") {
                if (value === "0") {
                    //reset
                    if (student) {
                        await student.roles.cache.forEach(async role => {
                            await student.roles.remove(role.id).catch(err => err);
                        });
                        await student.setNickname("");
                    }
                    studenti[studentNovell].discord = undefined;
                    if (interaction.guild.members)
                        writeFileSync("./jsons/studenti.json", JSON.stringify(studenti));
                    await interaction.editReply({ content: "U " + jmeno + " byl odstraněn " + parm, ephemeral: true }).catch(err => console.log(err));
                } else {
                    studenti[studentNovell].discord = value;
                    writeFileSync("./jsons/studenti.json", JSON.stringify(studenti));
                    await interaction.editReply({ content: "U " + jmeno + " byl zmněněn " + parm + " na " + value, ephemeral: true }).catch(err => console.log(err));
                    return;
                }
            }
            else { await interaction.editReply("Zadaný discord nebyl validný!").catch(err => console.log(err)); return; }
    } catch (error) {
        console.log(error);
    }


    //trida handeling-------------
    if (parm === "trida")
        if (skupiny[value.toLowerCase().replace(".", "")].fulljmeno === value || value === "0")
            if (value === "0") {
                //reset
                if (studenti[studentNovell].trida) {
                    const trida = await interaction.guild.roles.cache.find(role => role.name === studenti[studentNovell].trida);
                    const trida_admin = await interaction.guild.roles.cache.find(role => role.name === studenti[studentNovell].trida + "_admin");
                    if (trida != undefined) {
                        await student.roles.remove(trida);
                    }
                    if (trida_admin != undefined) {
                        await student.roles.remove(trida_admin);
                    }
                }
                studenti[studentNovell].trida = undefined;
                writeFileSync("./jsons/studenti.json", JSON.stringify(studenti));
                await interaction.editReply({ content: "U " + jmeno + " byla odstraněna " + parm, ephemeral: true }).catch(err => console.log(err));
            } else {

                //odstranit staré role třídy
                if (studenti[studentNovell].trida) {
                    const trida = await interaction.guild.roles.cache.find(role => role.name === studenti[studentNovell].trida);
                    const trida_admin = await interaction.guild.roles.cache.find(role => role.name === studenti[studentNovell].trida + "_admin");
                    if (trida != undefined) {
                        await student.roles.remove(trida);
                    }
                    if (trida_admin != undefined) {
                        await student.roles.remove(trida_admin);
                    }
                }

                //dát nové role třídy
                const trida = await interaction.guild.roles.cache.find(role => role.name === value);
                if (trida === undefined) {
                    await interaction.editReply("Vytvářím role...").catch(err => console.log(err));
                    const role = await interaction.guild.roles.create({
                        name: value,
                        color: [0, 255, 40],
                        reason: 'Role pro uživatele třídy',
                        hoist: true,
                        mentionable: true
                    }).catch(console.error);
                    const admin = await interaction.guild.roles.create({
                        name: value + "_admin",
                        color: 'AQUA',
                        reason: 'Role pro admina třídy'
                    }).catch(console.error);
                    const pO = [{
                        type: "role",
                        id: role.id,
                        allow: [PermissionFlagsBits.ViewChannel,
                        PermissionFlagsBits.SendMessages],
                    }, {
                        type: "role",
                        id: '1037064627571392553',
                        deny: [PermissionFlagsBits.ViewChannel],
                    }, {
                        type: "role",
                        id: admin.id,
                        allow: [PermissionFlagsBits.ManageRoles,
                        PermissionFlagsBits.ManageEmojisAndStickers,
                        PermissionFlagsBits.ManageChannels],
                    }];
                    await interaction.editReply("Vytvářím kanály...").catch(err => console.log(err));
                    const category = await interaction.guild.channels.create({
                        name: value,
                        type: ChannelType.GuildCategory,
                        permissionOverwrites: pO,
                    });
                    const text = await interaction.guild.channels.create({
                        name: "school chat",
                        type: ChannelType.GuildText,
                        permissionOverwrites: pO,
                        parent: category,
                    });
                    const voice = await interaction.guild.channels.create({
                        name: "general chat",
                        type: ChannelType.GuildVoice,
                        permissionOverwrites: pO,
                        parent: category,
                    });
                    roles[role.id] = category.id;
                    await student.roles.add(role);
                    await student.roles.add(admin);
                    writeFileSync("./jsons/roles.json", JSON.stringify(roles));
                    await interaction.editReply("Třída updatnuta na " + value + " a příslušné kanály vytvořeny.").catch(err => console.log(err));
                    await text.send("Vítej " + student.toString() + " v " + value + "!\n seš tu jako první, takže máš právo na:\n  -vytváření kanálů ve vaší kategorii\n  -**/backup** pro vytvoření zálohy z chatu");
                    studenti[studentNovell].trida = value;
                } else {
                    await student.roles.add(trida);
                    studenti[studentNovell].trida = value;
                }


                writeFileSync("./jsons/studenti.json", JSON.stringify(studenti));
                await interaction.editReply({ content: "U " + jmeno + " byla zmněněna " + parm + " na " + value, ephemeral: true }).catch(err => console.log(err));
                return;
            }
        else { await interaction.editReply("Zadaná třída nebyla validní!").catch(err => console.log(err)); return; }

}