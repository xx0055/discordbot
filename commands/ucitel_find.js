import { SlashCommandBuilder, PermissionFlagsBits, EmbedBuilder } from 'discord.js';
import studenti from '../jsons/studenti.json' assert { type: "json" };
import vyucujici from '../jsons/vyucujici.json' assert { type: "json" };

export const data = new SlashCommandBuilder()
    .setName('ucitel_find')
    .setDescription('Najdi učitele a jak ho žáci hodnotili')
    .setDefaultMemberPermissions(PermissionFlagsBits.CreatePrivateThreads)
    .addStringOption(option => option.setName('kantor')
        .setDescription('Zadej novellovský jméno nebo celé jméno kantora')
        .setRequired(true))
    .setDMPermission(false);
export async function execute(interaction) {
    var kantor = await interaction.options.getString("kantor");
    var hodnoceni;
    var pocet = {};
    const keysv = Object.keys(vyucujici);
    for (var i = 0; i < keysv.length; i++) {
        if (vyucujici[keysv[i]].fulljmeno === kantor) {
            kantor = keysv[i];
            break;
        }
    }
    if (!vyucujici[kantor]) {
        await interaction.reply({ content: "Kantora jsme nenašli...", ephemeral: true }).catch(err => console.log(err));
        return;
    }

    if (vyucujici[kantor].hodnoceni) { hodnoceni = vyucujici[kantor].hodnoceni; pocet = Object.keys(hodnoceni.znamka); } else {
        hodnoceni = {
            znamkacelk: "Nehodnoceno", spravedlnostcelk: "Nehodnoceno", kvalitacelk: "Nehodnoceno", chovanicelk: "Nehodnoceno",
            obtiznostcelk: "Nehodnoceno", ochotacelk: "Nehodnoceno"
        }; pocet.length = 0;
    }

    var description = "Email: ------------------- **" + kantor + "@spseplzen.cz**\n" +
        "Pracovní číslo: ---------- **" + vyucujici[kantor].cislo + "**\n" +
        "\n" +
        "**Hodnocení učitelů žáky:**\n" +
        "Celkové hodnocení: ---- **" + hodnoceni.znamkacelk + "**\n" +
        "Spravedlnost: ----------- **" + hodnoceni.spravedlnostcelk + "**\n" +
        "Kvalita výuky: ----------- **" + hodnoceni.kvalitacelk + "**\n" +
        "Chování vůči žákům: --- **" + hodnoceni.chovanicelk + "**\n" +
        "Obtížnost výuky: ------- **" + hodnoceni.obtiznostcelk + "**\n" +
        "Ochota: ----------------- **" + hodnoceni.ochotacelk + "**" +
        "\n\n**__Poznámky našich hlasujících__**:";
    if (hodnoceni.poznamka) {
        const poznamkujici = Object.keys(hodnoceni.poznamka);
        poznamkujici.forEach(async (poznamkovac) => {
            if (hodnoceni.poznamka[poznamkovac] != null && hodnoceni.poznamka[poznamkovac] != "")
                description += "\n__" + studenti[poznamkovac].fulljmeno + "__: \"*" + hodnoceni.poznamka[poznamkovac] + "*\"";
        });
    } else
        description += "\n*Zatím tu nic není*";
    const post = new EmbedBuilder()
        .setColor(0x0099FF)
        .setTitle(vyucujici[kantor].fulljmeno)
        .setDescription(description)
        .setFooter({ text: "Hodnotilo " + pocet.length + " lidí  //  Nalezeno během ... ms" });
    await interaction.reply({ embeds: [post], ephemeral: true }).catch(err => console.log(err));
    var ca = 0;
    await interaction.fetchReply().then(reply => ca = reply.createdTimestamp).catch(err => console.error(err));
    const ping = ca - interaction.createdAt + interaction.client.ws.ping;
    post.setFooter({ text: "Hodnotilo " + pocet.length + " lidí  //  Nalezeno během " + ping + "ms" });
    await interaction.editReply({ embeds: [post], ephemeral: true }).catch(err => console.log(err));
}