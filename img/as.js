const a = {
  type: 3,
  id: "1077646081929773156",
  applicationId: "1037063704895832105",
  channelId: "1042077625293733980",
  guildId: "1037064627571392553",
  user: {
    id: "543459815746306055",
    bot: false,
    system: false,
    flags: {
      bitfield: 4194304,
    },
    username: "xx0055",
    discriminator: "9459",
    avatar: "b37300283de90201fcc631da9077bef4",
    banner: undefined,
    accentColor: undefined,
  },
  member: {
    guild: {
      id: "1037064627571392553",
      name: "Prestižní SPŠE Plzeň",
      icon: "4b1a1bba7c8257899114bff1e05eb71a",
      features: [
        "MEMBER_VERIFICATION_GATE_ENABLED",
        "PREVIEW_ENABLED",
        "COMMUNITY",
        "NEWS",
        "AUTO_MODERATION",
      ],
      commands: {
        permissions: {
          manager: [Circular],
          guild: [Circular],
          guildId: "1037064627571392553",
          commandId: null,
        },
        guild: [Circular],
      },
      members: {
        guild: [Circular],
      },
      channels: {
        guild: [Circular],
      },
      bans: {
        guild: [Circular],
      },
      roles: {
        guild: [Circular],
      },
      presences: {
      },
      voiceStates: {
        guild: [Circular],
      },
      stageInstances: {
        guild: [Circular],
      },
      invites: {
        guild: [Circular],
      },
      scheduledEvents: {
        guild: [Circular],
      },
      autoModerationRules: {
        guild: [Circular],
      },
      available: true,
      shardId: 0,
      splash: null,
      banner: null,
      description: null,
      verificationLevel: 2,
      vanityURLCode: null,
      nsfwLevel: 0,
      premiumSubscriptionCount: 0,
      discoverySplash: null,
      memberCount: 177,
      large: true,
      premiumProgressBarEnabled: false,
      applicationId: null,
      afkTimeout: 300,
      afkChannelId: null,
      systemChannelId: "1042153743665332225",
      premiumTier: 0,
      widgetEnabled: null,
      widgetChannelId: null,
      explicitContentFilter: 2,
      mfaLevel: 0,
      joinedTimestamp: 1667325907038,
      defaultMessageNotifications: 1,
      systemChannelFlags: {
        bitfield: 13,
      },
      maximumMembers: 500000,
      maximumPresences: null,
      maxVideoChannelUsers: 25,
      approximateMemberCount: null,
      approximatePresenceCount: null,
      vanityURLUses: null,
      rulesChannelId: "1042153743665332224",
      publicUpdatesChannelId: "1042153743665332225",
      preferredLocale: "cs",
      ownerId: "543459815746306055",
      emojis: {
        guild: [Circular],
      },
      stickers: {
        guild: [Circular],
      },
    },
    joinedTimestamp: 1667325875094,
    premiumSinceTimestamp: null,
    nickname: "Jakub Svítek",
    pending: false,
    communicationDisabledUntilTimestamp: null,
    _roles: [
      "1045859234354581605",
      "1050469734832951366",
      "1053381004502511656",
      "1045791758128255017",
      "1045859144877482004",
      "1050134787416920116",
      "1053380987964358708",
    ],
    user: {
      id: "543459815746306055",
      bot: false,
      system: false,
      flags: {
        bitfield: 4194304,
      },
      username: "xx0055",
      discriminator: "9459",
      avatar: "b37300283de90201fcc631da9077bef4",
      banner: undefined,
      accentColor: undefined,
    },
    avatar: null,
  },
  version: 1,
  appPermissions: {
    bitfield: 4398046511103n,
  },
  memberPermissions: {
    bitfield: 4398046511103n,
  },
  locale: "cs",
  guildLocale: "cs",
  message: {
    channelId: "1042077625293733980",
    guildId: "1037064627571392553",
    id: "1077646071427235970",
    createdTimestamp: 1677001245124,
    type: 20,
    system: false,
    content: "Nalezeno 2 studentů",
    author: {
      id: "1037063704895832105",
      bot: true,
      system: false,
      flags: {
        bitfield: 0,
      },
      username: "Prestige Bot",
      discriminator: "7077",
      avatar: "79669d948186c4cde232773951b8a9bc",
      banner: undefined,
      accentColor: undefined,
      verified: true,
      mfaEnabled: false,
    },
    pinned: false,
    tts: false,
    nonce: null,
    embeds: [
      {
        data: {
          type: "rich",
          description: "Žák: **<@559308877288701972>/svitekm**\nTřída: **3.Q**",
          color: 39423,
        },
      },
      {
        data: {
          type: "rich",
          description: "Žák: **<@433260552878555165>/svitekj**\nTřída: **3.F**",
          color: 39423,
        },
      },
    ],
    components: [
      {
        data: {
          type: 1,
        },
        components: [
          {
            data: {
              type: 3,
              placeholder: "Nothing selected",
              options: [
                {
                  value: "svitekm",
                  label: "Martin Svítek",
                  description: "svitekm",
                },
                {
                  value: "svitekj",
                  label: "Jakub Svítek",
                  description: "svitekj",
                },
              ],
              min_values: 1,
              max_values: 1,
              custom_id: "student_vyber-0-svitek",
            },
          },
        ],
      },
      {
        data: {
          type: 1,
        },
        components: [
          {
            data: {
              type: 2,
              style: 1,
              label: "⬅️",
              custom_id: "student_minus-0-svitek",
            },
          },
          {
            data: {
              type: 2,
              style: 2,
              label: "🔄️",
              custom_id: "student_minus-10-svitek-1",
            },
          },
          {
            data: {
              type: 2,
              style: 1,
              label: "➡️",
              custom_id: "student_plus-0-svitek",
            },
          },
        ],
      },
    ],
    attachments: {
    },
    stickers: {
    },
    position: null,
    editedTimestamp: 1677001245341,
    reactions: {
      message: [Circular],
    },
    mentions: {
      everyone: false,
      users: {
      },
      roles: {
      },
      _members: null,
      _channels: null,
      _parsedUsers: null,
      crosspostedChannels: {
      },
      repliedUser: null,
    },
    webhookId: "1037063704895832105",
    groupActivityApplication: null,
    applicationId: "1037063704895832105",
    activity: null,
    flags: {
      bitfield: 64,
    },
    reference: null,
    interaction: {
      id: "1077646069812428851",
      type: 2,
      commandName: "student",
      user: {
        id: "543459815746306055",
        bot: false,
        system: false,
        flags: {
          bitfield: 4194304,
        },
        username: "xx0055",
        discriminator: "9459",
        avatar: "b37300283de90201fcc631da9077bef4",
        banner: undefined,
        accentColor: undefined,
      },
    },
  },
  customId: "student_vyber-0-svitek",
  componentType: 3,
  deferred: false,
  ephemeral: null,
  replied: false,
  webhook: {
    id: "1037063704895832105",
  },
  values: [
    "svitekm",
  ],
}