import { Client, Events, GatewayIntentBits, Collection, REST, Routes, ActivityType } from 'discord.js';
import config from './jsons/config.json' assert { type: "json" };
import roles from "./jsons/roles.json" assert { type: "json" };
import { readdirSync, appendFileSync, existsSync, mkdirSync, writeFileSync } from 'node:fs';
const rest = new REST({ version: '10' }).setToken(config.token);
import { change_group } from './commands/group_toggle.js';
import { change_kniha } from './commands/kniha.js';
import { student_minus, student_vyber, student_plus, student_upravit, student_handle } from './commands/student.js'
import { updateNovellChannel } from './lib/updateNovellChannel.js';

export const client = new Client({
	intents: [
		GatewayIntentBits.Guilds,
		GatewayIntentBits.GuildMembers,
		GatewayIntentBits.GuildPresences,
		GatewayIntentBits.MessageContent,
		GatewayIntentBits.GuildMessages
	]
});

export var presenceOptions = {
	activities: [{
		name: "čistou prestiž",
		type: ActivityType.Streaming,
		url: "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
	}],
	status: "idle"
}

export function changePresence(presenceOptionsInput) {
	presenceOptions = presenceOptionsInput;
}

try {
	const systemLogFile = Date.now() + '-system.log';


	// Upload comands -----------------------------------------------------
	client.commands = new Collection();
	const commandsPath = "./commands/";
	const commandFiles = readdirSync(commandsPath).filter(file => file.endsWith('.js'));
	const commands = [];

	(async () => {
		for (const file of commandFiles) {
			const filePath = commandsPath + file;
			const command = await import(filePath);
			try {
				if ('data' in command && 'execute' in command) {
					await client.commands.set(command.data.name, command);
				} else {
					console.log(`[WARNING] The command at ${filePath} is missing a required "data" or "execute" property.`);
				}
			} catch (error) {
				console.log(error);
			}

		}
		for (const file of commandFiles) {
			const command = await import(`./commands/${file}`);
			if (command.data)
				commands.push(command.data.toJSON());
		}
		try {
			console.log(`Started refreshing ${commands.length} application (/) commands.`);
			const data = await rest.put(
				Routes.applicationCommands(config.appid),
				{ body: commands },
			);
			console.log(`Successfully reloaded ${data.length} application (/) commands.`);
		} catch (error) {
			console.error(error);
		}
	})(); 


	//Imagine vituško---------------------
	console.log("Vituško je gay, imagine");

	//Příkaz handeling ---------------------------------------------------------
	client.on(Events.InteractionCreate, async (interaction) => {
		var reply;
		//await interaction.reply("Pracujeme na odpovědi....");
		if (interaction.isStringSelectMenu() || interaction.isButton() || interaction.isModalSubmit()) {
			if (interaction.customId.includes('cmd_group')) await change_group(interaction);
			if (interaction.customId.includes("cmd_kniha")) await change_kniha(interaction);
			if (interaction.customId.includes('student_minus')) await student_minus(interaction, interaction.customId);
			if (interaction.customId.includes('student_plus')) await student_plus(interaction, interaction.customId);
			if (interaction.customId.includes('student_vyber')) await student_vyber(interaction, interaction.customId);
			if (interaction.customId.includes('student_upravit')) await student_upravit(interaction, interaction.customId);
			if (interaction.customId.includes('student_handle')) await student_handle(interaction, interaction.customId);
			return;
		}
		await interaction.channel.sendTyping();
		if (interaction.guildId != '1037064627571392553') { await interaction.reply("Tento bot se smí používat jen na serveru!"); return; };
		if (!interaction.isChatInputCommand()) return;
		const command = await interaction.client.commands.get(interaction.commandName);
		if (!command) {
			console.error(`No command matching ${interaction.commandName} was found.`);
			return;
		}
		try {
			await command.execute(interaction).then(() => { setTimeout(async () => { await interaction.fetchReply().then(reply_ => reply = reply_).catch(err => console.error(err)); }, 200); }).catch(err => console.log(err));
		} catch (error) {
			console.error(error);
			await interaction.editReply({ content: 'Error při spouštění příkazu!', ephemeral: true }).catch(async (err) => { console.log(err); await interaction.reply({ content: 'Error při spouštění příkazu!', ephemeral: true }).catch(err => console.log(err)) });
			await interaction.fetchReply().then(reply_ => reply = reply_).catch(err => console.error(err));
		}

		//var reply = await interaction.channel.messages.fetch(interaction.channel.lastMessageId);
		var logOutput;
		const date = new Date(interaction.createdTimestamp + 3600000);
		try {
			setTimeout(async () => {
				await interaction.fetchReply().then(reply_ => reply = reply_).catch(err => console.error(err));
				if (interaction.channel) var channel = interaction.channel.name; else var channel = undefined;
				logOutput =
					`"${interaction.user.username}#${interaction.user.discriminator}" IN "${channel}" AT "${date.toUTCString()}":
"${interaction.toString()}"
Bot response:
"${reply}"`
				if (reply && reply.embeds[0]) logOutput += `
Embedy:
"${JSON.stringify(reply.embeds[0].data)}"`
				if (reply && reply.components[0]) logOutput += `
Komponenty:
"${JSON.stringify(reply.components[0].components[0].data)}"`
				logOutput += "\n\n";
				appendFileSync(
					"./backups/system/" + systemLogFile,
					logOutput
				);
				appendFileSync(
					"system.log",
					logOutput
				)
			}, 500);
		} catch (error) {
			console.log(error)
		}

	});

	//on start ------------------------------------------------------
	client.once(Events.ClientReady, async c => {

		const guild = (await c.guilds.fetch("1037064627571392553")).channels.guild;
		//const moderatorChat = (await guild.channels.fetch("1042153743665332225")).send("discord")
		console.log("Getting the bot ready!")
		if (!existsSync("./backups/")) {
			mkdirSync("./backups/");
		}
		if (!existsSync("./backups/system/")) {
			mkdirSync("./backups/system/");
		}
		writeFileSync("./backups/system/" + systemLogFile, "");
		var channelsDeleted = 0;
		var rolesDeleted = 0;
		guild.roles.cache.forEach(async (role) => {
			if (role.members.size == 0) {
				if (roles[role.id]) {
					guild.channels.cache.forEach(async (channel) => {
						if (channel.id === roles[role.id] || channel.parentId === roles[role.id]) {
							channel.delete("Empty Channel - startup");
							channelsDeleted++;
						}
					})
					delete roles[role.id];
					role.delete("0 members - startup");
					rolesDeleted++;
				}
			}
		});
		writeFileSync("./jsons/roles.json", JSON.stringify(roles));
		console.log("Odstraněno " + rolesDeleted + " rolí a " + channelsDeleted + " kanálů protože byly prázdný!");

		await c.user.setPresence(presenceOptions);

		// --------------------- 1m cock ---------------------
		setInterval(async () => {
			await c.user.setPresence(presenceOptions);

			// ---------------- weekly cock -------------------
			if (config.nextWeeklyNotification < Date.now()) {
				config.nextWeeklyNotification += 604800000;

				//připomínka objednání jídel
				//if (new Date(Date.now()).getMonth() != 7 && new Date(Date.now()).getMonth() != 8)
				//	await jidlaChannel.send("<@&1053380987964358708> - Nezapomeň si objednat jídlo na příští tejden!");

				await updateNovellChannel(novellUpdateChannel, true);
				writeFileSync("./jsons/config.json", JSON.stringify(config));
			}

		}, 60000);
		const loginChannel = await guild.channels.fetch("1045743935374508042");
		const jidlaChannel = await guild.channels.fetch("1053391061579464794");
		const redditChannel = await guild.channels.fetch("1049983003113304104");
		const novellUpdateChannel = await guild.channels.fetch("1065768354750668940");
		await updateNovellChannel(novellUpdateChannel);
		// --------------------- 10h cock ---------------------
		setInterval(async () => {
			//clear #login
			await loginChannel.messages.fetch();
			await loginChannel.messages.cache.forEach(async (message) => {
				try {
					if (message.author != "543459815746306055")
						//console.log("Zpráva od " + message.author + ": " + message.content + " v #login - to je kokot xd");
						await message.delete();

				} catch (error) {
					//console.log(error);
				}
			})

			//update zaky, ucitele a tridy
			await updateNovellChannel(novellUpdateChannel);

		}, 36000000);

		////------------Reaload oborů -----------------------------
		/*
		var tD = new TextDecoder()
		const keyss = Object.keys(studenti);
		guild.members.fetch();
		guild.members.cache.forEach(async (member) => {
			for (var i = 0; i < keyss.length - 1; i++) {
				if (studenti[keyss[i]].fulljmeno === member.displayName) {
					if (!skupiny[studenti[keyss[i]].trida]) {
						await get.concat({ url: 'http://mail.spseplzen.cz/SOGo/so/svitekj/Contacts/SPSE_adresy/' + keyss[i] + '/view', method: 'GET', headers: sogo_headers }, async (err, res, data) => {
							if (err) throw err;
							var string = tD.decode(data);
							//console.log(string);
							data = await JSON.parse(string);
							const tridaString = data.addresses[0].locality;
							studenti[keyss[i]].trida = tridaString;
							var zamereni;
							if (skupiny[studenti[keyss[i]].trida.toLowerCase().replace(".", "")].obor)
								zamereni = skupiny[studenti[keyss[i]].trida.toLowerCase().replace(".", "")].obor.slice(0, 10);
							switch (zamereni) {
								case "18-20-M/01":
									await member.roles.add(guild.roles.cache.get("1050134787416920116"));
									break;
								case "26-41-M/01":
									await member.roles.add(guild.roles.cache.get("1050135125486211102"));
									break;
								case "78-42-M/01":
									await member.roles.add(guild.roles.cache.get("1050135113494691920"));
									break;
	
								default:
									console.log("zaměření " + zamereni + " nebylo nalezeno - u uživatele " + studenti[keyss[i]] + "/" + member.toString());
									break;
							}
	
						});
					} else {
						var zamereni;
							if (skupiny[studenti[keyss[i]].trida.toLowerCase().replace(".", "")].obor)
								zamereni = skupiny[studenti[keyss[i]].trida.toLowerCase().replace(".", "")].obor.slice(0, 10);
						switch (zamereni) {
							case "18-20-M/01":
								await member.roles.add(guild.roles.cache.get("1050134787416920116"));
								break;
							case "26-41-M/01":
								await member.roles.add(guild.roles.cache.get("1050135125486211102"));
								break;
							case "78-42-M/01":
								await member.roles.add(guild.roles.cache.get("1050135113494691920"));
								break;
	
							default:
								console.log("zaměření " + zamereni + " nebylo nalezeno - u uživatele " + studenti[keyss[i]] + "/" + member.toString());
								break;
						}
					}
	
				}
			}
		})
		*/
		//------------Reload Učitelských embedů----------------
		/*
		const keysv = Object.keys(vyucujici);
		var uciteleChannel = await guild.channels.fetch("1048348552469958775"); 
		
		keysv.forEach(async (kantor) =>{if(!vyucujici[kantor].hodnoceni)return;
			await uciteleChannel.messages.fetch()
			var ucitelMessage;
			await uciteleChannel.messages.cache.forEach(async (message) =>{
				if (message.embeds[0].title === vyucujici[kantor].fulljmeno) {
					ucitelMessage = message;
					return;
				}
			})
		const hodnoceni = vyucujici[kantor].hodnoceni
		const pocet = Object.keys(hodnoceni.znamka);
		var description = "Email: ------------------- **" +kantor + "@spseplzen.cz**\n"+
				"Pracovní číslo: ---------- **" +vyucujici[kantor].cislo + "**\n"+
				"\n"+
				"**Hodnocení učitelů žáky:**\n"+
				"Celkové hodnocení: ---- **" +hodnoceni.znamkacelk + "**\n"+
				"Spravedlnost: ----------- **" +hodnoceni.spravedlnostcelk + "**\n"+
				"Kvalita výuky: ----------- **" +hodnoceni.kvalitacelk + "**\n"+
				"Chování vůči žákům: --- **" +hodnoceni.chovanicelk + "**\n"+
				"Obtížnost výuky: ------- **" +hodnoceni.obtiznostcelk + "**\n"+
				"Ochota: ----------------- **" +hodnoceni.ochotacelk + "**"+
				"\n\n**__Poznámky našich hlasujících__**:";
				if(hodnoceni.poznamka){
					const poznamkujici = Object.keys(hodnoceni.poznamka);
					poznamkujici.forEach(async (poznamkovac) =>{
						description+="\n__"+studenti[poznamkovac].fulljmeno+"__: \"*"+hodnoceni.poznamka[poznamkovac]+"*\"";
					})
				}else description+="\n*Zatím tu nic není*"
				const post = new EmbedBuilder()
					.setColor(0x0099FF)
					.setTitle(vyucujici[kantor].fulljmeno)
					.setDescription(description)
					.setFooter({text: "Hodnotilo " + pocet.length + " lidí"});
	
				if(ucitelMessage){
					ucitelMessage.edit({ embeds: [post] });
				}else{
					
					await uciteleChannel.send({ embeds: [post] });
				}
				
			})
		*/

		console.log(`Ready! Logged in as ${c.user.tag} and ready to go`);
	});

	client.login(config.token);
} catch (error) {
	console.error(error);
} finally {

}